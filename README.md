# grab.py
### A simple tool for downloading content for archival purposes

# NOTE!
This project is currently in prototyping phase and is not ready
For production. Use at your own risk.

#### Goals
1. Lightweight 
2. Support wide verity of formatting options
3. Participate in the unix phelosophy. 
- - -
### Installation
1. clone repository
2. install dependencies
3. export script to PATH.
- - -
### Dependencies
  - [pandoc][1]
  - See [requirements.txt][2]

### Usage:
Currently this is just a template of what I plan on implementing. So far only
URL is implemented in the prototype. 

Not all features noted below have been implemented. 
```
Usage: grab [options] URL

Scrape content from web-pages to STDOUT or a file.

positional arguments:
  URL                 Target URL

optional arguments:
  -h, --help          show this help message and exit
  -w [Filename]       Write to file
  -t [Output_Format]  Specify document format (See man(1) pandoc)
  -v                  Verbose Output

This program is considered a prototype. The author is not responsible for
anything that happens to you or your property when running this program.

```

[1]: http://pandoc.org/
[2]: ./requirements.txt
